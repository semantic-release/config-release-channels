import test from 'ava';

test('Can load the ECMAscript module', async t => {
	const {default: {branches}} = await import('../config.mjs');

	t.is(branches.length, 4);
});
