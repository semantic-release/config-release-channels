// eslint-disable-next-line import/order
import {createRequire} from 'node:module';

const require = createRequire(import.meta.url);
const cfg = require('./config.json');

export default cfg;
