# @semantic-release/config-release-channels

> [**semantic-release**](https://github.com/semantic-release/semantic-release)
> config to setup `alpha`, `beta` and `stable` release channels.

## Getting Started

Add the following to `.releaserc.yaml`:

```yaml
extends:
  - "@semantic-release/config-release-channels"
```

Combine that with other plugins or configurations:

```yaml
extends:
  - "@semantic-release/config-release-channels"
  - "@semantic-release/config-language-specific"
```

## Configuration

### Branches

Create the `alpha`, `beta` and `stable` branches to the root commit:

```sh
for BRANCH in alpha beta stable; do
  git checkout -b $BRANCH $(git rev-list --max-parents=0 HEAD)
  git push -u origin $BRANCH
done
```

### Protect

#### Branches

Protecting the release branches enables protected CI variables to be allowed in the `semantic-release` jobs.

For each release branch [protect][protect-branch] the branch.

Recommended protections are for _no-one_ to be able to merge and for only maintainers to _merge_.

#### Tags

`semantic-release` will create version tags.

It is sensible to protect the version tags against accidental deletion.

Create the `v*` wildcard [protection][protect-tag].

## Usage

All work happens on `main`. When a release needs to be made, a merge can be made
to `alpha`:

    # We will do an 'alpha' release first
    git checkout alpha

    # Pull in all the changes for the release
    git merge --no-ff main

    # Push up the 'alpha' branch for release on the CI
    git push

`semantic-release` will automatically pick up the changes on the `alpha` branch
and create a new `N.N.N-alpha.X` release.

We can make multiple `alpha` releases, until we are comfortable we want to
promote the changes to `beta`. The same process is followed:

    # Move 'alpha' changes to 'beta'
    git checkout beta
    git merge --no-ff main
    git push

This will result in a `N.N.N-beta.N` release.

Finally, when we are happy with the stability of the `beta` channel we can
promote to `stable`:

    # Move 'beta' changes to 'stable'
    git checkout stable
    git merge --no-ff main
    git push

This will create the final `N.N.N` release for the changes. The final release
can be merged back to the prerelease branches so that subsequent prereleases are
for the _next_ major version:

    # Apply stable release back to the prerelease channels
    git checkout alpha
    git merge --no-ff vN.N.N
    git push
    git checkout beta
    git merge --no-ff vN.N.N
    git push

[protect-branch]: https://docs.gitlab.com/ee/user/project/protected_branches.html
[protect-tag]: https://docs.gitlab.com/ee/user/project/protected_tags.html
